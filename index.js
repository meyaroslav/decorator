// Декоратор для получения массива сервисов модуля
function getServiceArray(module) {
    // Проверяем, является ли переданный аргумент объектом
    if (typeof module !== 'object' || module === null) {
        throw new Error('Ошибка');
    }
    const properties = Object.getOwnPropertyNames(module);
    const modules = properties.filter(property => typeof module[property] === 'function');
    return modules;
}
// Пример использования декоратора
const myModule = {
    module1() {
    },
    module2() {
    },
    data: {
        // Данные модуля
    }
};
const modules = getServiceArray(myModule);
console.log(modules);

